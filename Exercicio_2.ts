/* A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente 
a um trabalho de laboratório, a uma avaliação semestral e a um exame final. A média das três 
notas mencionadas anteriormente obedece aos pesos a seguir. */

namespace Exercicio_2

{

    let nota1, nota2, nota3, peso_tra, peso_ava, peso_exa, media_p:number;

    nota1 = 3
    nota2 = 5
    nota3 = 7
    peso_tra = 2;
    peso_ava = 3;
    peso_exa = 5;

    media_p = (nota1 * peso_tra + nota2 * peso_ava + nota3 * peso_exa) / (peso_tra + peso_ava + peso_exa);

    if(media_p >=8 && media_p <=10)

    {
        console.log ("Termo A");
    }

    if(media_p >=7 && media_p <=8)

    {
        console.log ("Termo B");
    }

    if(media_p >=6 && media_p <=7)

    {
        console.log ("Termo C");
    }

    if(media_p >=5 && media_p <=6)

    {
    console.log ("Termo D");
    }

    if(media_p >=0 && media_p <=5)

    {   
    console.log ("Termo E");
    }

}
