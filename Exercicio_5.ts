namespace exercicio_5

{
    
    const data = new Date();
    const hora = data.getHours();

    const nome = "Gabriel mais lindo do mundo"; 

    if (hora >= 6 && hora <= 12) 
    {
        console.log("Bom dia " + nome);
    }
    else if (hora >= 12 && hora <= 17)
    {
        console.log("Boa tarde " + nome);
    }
    else 
    {
        console.log ("Boa noite " + nome);
    }

}